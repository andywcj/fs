namespace FlightSchedule.Dao.Interface
{
    public interface IDao
    {
        IOrderDao iOrderDao { get; }
        IFlightDao iFlightDao { get; }
    }
}