using System.Collections.Generic;
using System.Threading.Tasks;
using FlightSchedule.Model;

namespace FlightSchedule.Dao.Interface
{
    public interface IFlightDao
    {
        Task<List<Flight>> GetFlights();
    }
}