using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using FlightSchedule.Dao.Interface;
using FlightSchedule.Model;
using Newtonsoft.Json;

namespace FlightSchedule.Dao
{
    public class FlightDao : IFlightDao
    {
        public  async Task<List<Flight>> GetFlights()
        {
            List<Flight> flights = new List<Flight>();

            try
            {
                using (StreamReader r = new StreamReader("./Config/Flights.json"))
                {
                    string json = await r.ReadToEndAsync();

                    FlightPlan flightPlan = JsonConvert.DeserializeObject<FlightPlan>(json);

                    flights = flightPlan.flights;
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine("Parse json file fail" + ex);
            }

            return flights;
        }
    }
}