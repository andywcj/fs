using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using FlightSchedule.Dao.Interface;
using FlightSchedule.Model;
using Newtonsoft.Json.Linq;

namespace FlightSchedule.Dao
{
    public class OrderDao : IOrderDao
    {
        public async Task<List<Order>> GetOrders()
        {
            List<Order> orders = new List<Order>();

            try
            {
                using (StreamReader r = new StreamReader("./Config/coding-assigment-orders.json"))
                {
                    string json = await r.ReadToEndAsync();
                    JObject jsonObj = JObject.Parse(json);

                    foreach (JProperty jProperty in jsonObj.Properties())
                    {
                        Order order = new Order();

                        order.id = jProperty.Name;
                        order.origin = "YUL";

                        JToken value = jProperty.Value;
                        if (value.Type == JTokenType.Object)
                        {
                            JObject destination = (JObject)value;
                            order.destination = (string)destination["destination"];
                        }

                        orders.Add(order);
                    }
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine("Parse json file fail" + ex);
            }

            return orders;
        }
    }
}