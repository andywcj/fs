using FlightSchedule.Dao.Interface;

namespace FlightSchedule.Dao
{
    public class BaseDao : IDao
    {
        public IOrderDao iOrderDao
        {
            get
            {
                return new OrderDao();
            }
        }

        public IFlightDao iFlightDao
        {
            get
            {
                return new FlightDao();
            }
        }
    }
}