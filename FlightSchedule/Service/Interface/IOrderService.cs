using System.Collections.Generic;
using System.Threading.Tasks;
using FlightSchedule.Model;

namespace FlightSchedule.Service.Interface
{
    public interface IOrderService : IService
    {
        Task PrintOrders();
    }
}