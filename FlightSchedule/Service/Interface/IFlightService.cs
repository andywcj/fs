using System.Threading.Tasks;

namespace FlightSchedule.Service.Interface
{
    public interface IFlightService
    {
        Task PrintFlights();
    }
}