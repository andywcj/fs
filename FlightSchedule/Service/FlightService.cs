using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FlightSchedule.Dao.Interface;
using FlightSchedule.Model;
using FlightSchedule.Service.Interface;

namespace FlightSchedule.Service
{
    public class FlightService : IFlightService
    {
        protected IDao iDao;

        public FlightService(IDao iDao)
        {
            this.iDao = iDao;
        }

        public async Task PrintFlights()
        {
            List<Flight> flights = await iDao.iFlightDao.GetFlights();

            foreach (Flight f in flights)
            {
                Console.WriteLine("Flight: {0}, departure: {1}, arrival: {2}, day: {3}", f.id, f.origin, f.destination, f.date);
            }
        }
    }
}