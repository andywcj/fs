using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FlightSchedule.Dao;
using FlightSchedule.Dao.Interface;
using FlightSchedule.Model;
using FlightSchedule.Service.Interface;

namespace FlightSchedule.Service
{
    public class OrderService : IOrderService
    {
        protected IDao iDao;

        public OrderService(IDao iDao)
        {
            this.iDao = iDao;
        }

        private async Task<List<Order>> ScheduleOrderAsync()
        {
            List<Order> orders = await iDao.iOrderDao.GetOrders();
            List<Flight> flights = await iDao.iFlightDao.GetFlights();


            Dictionary<string, List<Flight>> dictionary = new Dictionary<string, List<Flight>>();

            foreach (Flight f in flights)
            {
                if (dictionary.ContainsKey(f.destination))
                {
                    Flight flight = new Flight();
                    flight.id = f.id;
                    flight.capacity = f.capacity;
                    flight.date = f.date;
                    dictionary[f.destination].Add(flight);
                }
                else
                {
                    List<Flight> flightList = new List<Flight>();
                    Flight flight = new Flight();
                    flight.id = f.id;
                    flight.capacity = f.capacity;
                    flight.date = f.date;
                    flightList.Add(flight);
                    dictionary.Add(f.destination, flightList);
                }
            }

            foreach (Order o in orders)
            {
                if (dictionary.ContainsKey(o.destination) && dictionary[o.destination].Count > 0)
                {
                    o.flightId = dictionary[o.destination][0].id;
                    o.flightDate = dictionary[o.destination][0].date;
                    dictionary[o.destination][0].capacity--;
                    if (dictionary[o.destination][0].capacity == 0)
                        dictionary[o.destination].RemoveAt(0);
                }
            }

            return orders;
        }

        public async Task PrintOrders()
        {
            List<Order> orders = await ScheduleOrderAsync();

            foreach (Order o in orders)
            {
                if (o.flightDate == 0)
                    Console.WriteLine("order: {0}, flightNumber: not scheduled", o.id);
                else
                {
                    Console.WriteLine("order: {0}, flightNumber: {1}, departure: {2}, arrival:{3}, day: {4}", o.id, o.flightId, o.origin, o.destination, o.flightDate);
                }
            }
        }
    }
}