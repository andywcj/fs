﻿using System;
using System.Threading.Tasks;
using FlightSchedule.Dao;
using FlightSchedule.Dao.Interface;
using FlightSchedule.Service;

namespace FlightSchedule
{
    class Program
    {
        static async Task Main(string[] args)
        {
            Console.WriteLine("Press 1 to see User story #1, press 2 to see User Story #2, press 0 to exit");

            string input = Console.ReadLine();

            while (input != "0")
            {
                IDao iDao = new BaseDao();

                switch (input)
                {
                    case "1":
                        {
                            FlightService flightService = new FlightService(iDao);
                            await flightService.PrintFlights();
                            break;
                        }
                    case "2":
                        {
                            OrderService service = new OrderService(iDao);
                            await service.PrintOrders();
                            break;
                        }
                    default:
                        {
                            Console.WriteLine("Invalid input, please try again");
                            break;
                        }
                }

                Console.WriteLine("Press 1 to see User story #1, press 2 to see User Story #2, press 0 to exit");
                input = Console.ReadLine();
            }

        }
    }
}
