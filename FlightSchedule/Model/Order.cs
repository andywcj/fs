namespace FlightSchedule.Model
{
    public class Order
    {
        public string id { get; set; }
        public string origin { get; set; }
        public string destination { get; set; }
        public int flightId { get; set; }
        public int flightDate { get; set; }
    }
}