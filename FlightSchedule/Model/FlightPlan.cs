using System.Collections.Generic;

namespace FlightSchedule.Model
{
    public class FlightPlan
    {
        public FlightPlan()
        {
            flights = new List<Flight>();
        }
        public List<Flight> flights { get; set; }
    }
}