namespace FlightSchedule.Model
{
    public class Flight
    {
        public int id { get; set; }
        public string origin { get; set; }
        public string destination { get; set; }
        public int date { get; set; }
        public int capacity { get; set; }
    }
}